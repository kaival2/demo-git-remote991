


<?php 
class Mahasiswa_model{
    // private $mhs = [
    //     [
    //         "Nama" => "Kaisar Fredi",
    //         "NRP" => "17324746872",
    //         "Email" => "kaisar@gmail.com",
    //         "Jurusan" => "Informatika"
    //     ],
    //     [
    //         "Nama" => "Aldi Ferdian",
    //         "NRP" => "17353756872",
    //         "Email" => "aldi@gmail.com",
    //         "Jurusan" => "Teknik Mesin"
    //     ],
    //     [
    //         "Nama" => "Galih Agus",
    //         "NRP" => "17324703877",
    //         "Email" => "galih@gmail.com",
    //         "Jurusan" => "Akuntansi"
    //     ],
    //     [
    //         "Nama" => "Imam Mahdi",
    //         "NRP" => "17324743212",
    //         "Email" => "imam@gmail.com",
    //         "Jurusan" => "Manajemen"
    //     ]
    // ];

    // private $dbh; //data source handler
    // private $stmt;

    // public function __construct(){
    //     //data source name
    //     $dsn = 'mysql:host=localhost;dbname=phpmvc';

    //     try{
    //         $this->dbh = new PDO($dsn, 'root', '');
            
    //     }catch(PDOException $e){
    //         die($e->getMessage());
    //     }
    // }

    private $table = "mahasiswa";
    private $db;

    public function __construct (){
        $this->db = new Database();
    }

    public function getAllMahasiswa(){
        // $this->stmt = $this->dbh->prepare('SELECT * FROM mahasiswa');
        // $this->stmt->execute();

        // return $this->stmt->fetchAll(PDO::FETCH_ASSOC);

        $this->db->query('SELECT * FROM '. $this->table);

        return $this->db->resultSet();
    }

    public function getMahasiswaById($id){
        $this->db->query('SELECT * FROM '. $this->table. ' WHERE id=:id');

        $this->db->bind('id', $id);

        return $this->db->single();
    }
}


?>