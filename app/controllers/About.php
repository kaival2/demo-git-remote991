<?php 
class About extends Controller{

    //Method default
    public function index($nama = "Kaisar", $job = "Programmer", $umur = 19){
        $data['nama'] = $nama;
        $data['umur'] = $umur;
        $data['pekerjaan'] = $job;
        $data['judul'] = "About";

        $this->view('templates/header', $data);
		$this->view('about/index', $data);
        $this->view('templates/footer');

	}
    //Method
    public function page(){
        $this->view('about/page');

    }

}

?>